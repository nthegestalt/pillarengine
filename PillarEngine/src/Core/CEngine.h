#ifndef CENGINE_H_
#define CENGINE_H_

#include "global.h"
#include "IDemo.h"

class CEngine
{
public:
	static CEngine* GetEngine(unsigned int window_width, unsigned int window_height,
		unsigned int glMajorVersion, unsigned int glMinorVersion,
		char* windowTitle);

	void Setup(bool fullScreenEnable);
	void AttachDemo(IDemo* demoInstance);
	void Run();
	void Cleanup();

private:
	CEngine(unsigned int window_width, unsigned int window_height, 
			  unsigned int glMajorVersion, unsigned int glMinorVersion,
			  char* windowTitle);
	~CEngine();

private:
	static void window_size_callback(GLFWwindow* window, int width, int height);
	void Keyboard();
	void Mouse();

private:
	bool m_isFullScreen;
	static int m_windowWidth;
	static int m_windowHeight;
	unsigned int m_glMinorVersion;
	unsigned int m_glMajorVersion;

	string m_windowTitle;
	IDemo* m_demoInstance;
	GLFWwindow *m_window;

private:
	static CEngine* m_instance;
};

#endif