#ifndef CSCENEMANAGER_H_
#define CSCENEMANAGER_H_

#include <stdlib.h>
#include <vector>
#include <iostream>
#include "global.h"
#include "IObject.h"

using std::vector;
using std::iostream;

/**
	Every Demo has it's own scene manager object which takes care of initialisation, rendering and cleanup
	of all demo objects we have.
*/

class CSceneManager
{
public:
	CSceneManager();
	~CSceneManager();

	// Core functions
	void InitScene();
	void DrawScene();

	//Note: This will not* truly destroy the scene manager as it exists as a singleton
	//		  in each of the demo instances. To destroy the actual scene manager, we need
	//      to call the "DeleteSceneManager()" function that is inherited by the demo instance.
	//      For more details refer to files: SceneManagerInstance.cpp and IDemo.h.
	void ClearObjects(); 

	// Auxillary Scene management
	void     AddObject(IObject* obj);
	IObject* GetObject(int objID);
	void     RemoveObject(int objID);
	void     ListCurrentObjects();

private:
	vector<IObject*> m_SceneObjects;
};

#endif