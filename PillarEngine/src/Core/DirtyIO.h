#ifndef DIRTYIO_H_
#define DIRTYIO_H_

#include "global.h"
/**
	Some dirty functions I defined for GLFW keyboard input to make input code cleaner.
*/

struct KeyObj
{
	int number;
	bool isPressed;
};

/**
	Dirty function for key input which avoids "multi-fire".
	Don't expect to use any functions that returns values here,
	we simply just wish to call a void function on a key press.

	How to use:
	- Define the following function pointer:
	  auto fptr = std::bind(&class::action, action(input1,input2,input-n));

   - Call the following function:
	  KeyPressOnce(GLFW window, Key Binding Obj, function);

	@Params: (ptr-obj) window, (ref-struct) key, (ptr-mb-fnc) action function (C++11 feature)
*/
static void KeyPressOnce(GLFWwindow* w, KeyObj &key, std::function<void()> action)
{
	// We poll to see if the key has been pressed. Once it's been pressed, we set the flag
	// to represent the state. While the flag is set for this key, the action is no longer
	// triggered until we release which resets the flag. This is a work around to GLFW's
	// lack of single-press handling.
	if (glfwGetKey(w, GLFW_KEY_L) == GLFW_PRESS)
	{
		if (key.isPressed == false)
		{
			key.isPressed = true;
			action();
		}
	}

	if (glfwGetKey(w, GLFW_KEY_L) == GLFW_RELEASE)
	{
		key.isPressed = false;
	}
}
#endif