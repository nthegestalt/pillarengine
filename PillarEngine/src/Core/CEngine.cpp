#include "CEngine.h"

// Static decleration
int CEngine::m_windowWidth = 0;
int CEngine::m_windowHeight = 0;
CEngine* CEngine::m_instance = 0;

/*
	Grabs singleton instance
*/
CEngine* CEngine::GetEngine(unsigned int window_width, unsigned int window_height,
								    unsigned int glMajorVersion, unsigned int glMinorVersion,
									 char* windowTitle)
{
	if (m_instance == NULL)
	{
		m_instance = new CEngine(800, 600, 3, 3, "Demo Engine");
	}

	return m_instance;
}

/**
	Default constructor
*/
CEngine::CEngine(unsigned int window_width, unsigned int window_height,
					  unsigned int glMajorVersion, unsigned int glMinorVersion,
					  char* windowTitle)
{
	
	this->m_windowWidth = window_width;
	this->m_windowHeight = window_height;
	m_glMinorVersion = glMinorVersion;
	m_glMajorVersion = glMajorVersion;
	m_windowTitle = windowTitle;
	m_demoInstance = NULL;
}

CEngine::~CEngine()
{

}

/**
	Attach given demo to run
	@Params: Demo
	@Returns: Nil
*/
void CEngine::AttachDemo(IDemo* demo)
{
	m_demoInstance = demo;
}

/**
	Setup the engine
	@Params: full screen?
	@Returns: Nil
*/
void CEngine::Setup(bool fullScreenEnable)
{
	// Initialize glfw
	if (!glfwInit())
	{
		fprintf(stderr, "Error: Failed to initialize glfw.\n");
		system("pause");
		exit(EXIT_FAILURE);
	}

	glfwWindowHint(GLFW_SAMPLES, 64);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, m_glMajorVersion);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, m_glMinorVersion);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	m_window = glfwCreateWindow(m_windowWidth, m_windowHeight, m_windowTitle.c_str(), fullScreenEnable ? glfwGetPrimaryMonitor() : NULL, NULL);
	if (!m_window)
	{
		glfwTerminate();
		fprintf(stderr, "Error: Could not create window context.\n");
		system("pause");
		exit(EXIT_FAILURE);
	}

	glfwSetWindowSizeCallback(m_window, window_size_callback);
	glfwMakeContextCurrent(m_window);

	// Initialize GLEW
	glewExperimental = GL_TRUE; // Needed for core profile
	GLenum err = glewInit();
	if (err != GLEW_OK)
	{
		fprintf(stderr, "Failed to initialize GLEW\n");
		fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
		system("pause");
		exit(EXIT_FAILURE);
	}

	// Setup our attached demo if it exists
	if (m_demoInstance != NULL)
	{
		m_demoInstance->Setup();
		printf("Demo Detected: %s\n", m_demoInstance->GetName().c_str());
	}
}

/**
	Runs the engine and associated demo(s)
	@Params: Nil
	@Returns: Nil
*/
void CEngine::Run()
{
	while (!glfwWindowShouldClose(m_window))
	{
		int w, h;
		w = m_windowWidth;
		h = m_windowHeight;

		glfwGetFramebufferSize(m_window, &w, &h);

		/* Magic! */
		if (m_demoInstance != NULL) {
			m_demoInstance->Run();
		}

		Mouse();
		Keyboard();

		glfwSwapBuffers(m_window);
		glfwPollEvents();
	}

	// In case we click x instead of escape
	this->Cleanup();
}

/**
	Handles keyboard events
	@Params: Nil
	@Returns: Nil
*/
void CEngine::Keyboard()
{
	//Process the demo keyboard input if any
	if (m_demoInstance != NULL)
		m_demoInstance->Keyboard(m_window);

	//If KeyPressed == ESC, stop the engine and exit
	if (glfwGetKey(m_window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
	{
		printf("Shutting down Engine\n");
		glfwSetWindowShouldClose(m_window, GL_TRUE);
		this->Cleanup();
	}
}

/**
	Handles mouse events
	@Params: Nil
	@Returns: Nil
*/
void CEngine::Mouse()
{
	if (m_demoInstance != NULL)
	{
		m_demoInstance->Mouse(m_window);
	}
}

/**
	Cleans everything up
	@Params: Nil
	@Returns: Nil
*/
void CEngine::Cleanup()
{
	if (m_demoInstance != NULL)
	{
		m_demoInstance->Cleanup();

		delete m_demoInstance;
		m_demoInstance = NULL;
	}

	glfwDestroyWindow(m_window);
	glfwTerminate();
	
	delete m_instance;
	m_instance = NULL;

	exit(EXIT_SUCCESS);
}

/**
	Resizes window viewport
	@Params: window width, window height
	@Returns: Nil
*/
void CEngine::window_size_callback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
	m_windowWidth = width;
	m_windowHeight = height;

	printf("Window size is: %i x %i pixels\n", m_windowWidth, m_windowHeight);
}