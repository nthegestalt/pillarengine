#ifndef COREFUNCTIONTESTS_H_
#define COREFUNCTIONTESTS_H_

/**
	Misc tests I wrote to make sure engine works as intended during development.
*/

#include "CSceneManager.h"
#include "Object\Geometry\CDummyObj.h"
#include "Demo\DummyDemo\CDummyDemo.h"

/**
	Tests the following:
	-	Scene manager class is working properly
	-	Object derivation is working properly i.e. chain of function/constructor calls are 
		being made based on the order of inheritance.
*/
void RunSceneManagerTest()
{
	printf("\nRunning Scene Manager and Object Unit Tests...\n");
	CSceneManager* manager = new CSceneManager();

	CDummyObj* obj = new CDummyObj();
	obj->SetType("Geometric Object");
	obj->SetDescription("Illuminati!");

	manager->AddObject(obj);
	manager->InitScene();
	manager->DrawScene();
	manager->ClearObjects();

	obj = NULL;

	delete manager;
	manager = NULL;
}

/**
	Tests the following:
	-	Scene manager singleton instance is working properly i.e. we have two scene managers
		instead of one for each demo instance.
	-	Function calls are being carried out correctly
*/
void RunDemotest()
{
	printf("\nRunning Demo Object Unit Tests...\n");
	CDummyObj* obj = new CDummyObj();
	obj->SetType("Geometric Object");
	obj->SetDescription("Illuminati!");

	CDummyObj* obj2 = new CDummyObj();
	obj2->SetType("Geometric Object");
	obj2->SetDescription("HL3 Confirmed!");

	CDummyDemo* demo = new CDummyDemo("Hello World Demo!");
	demo->Setup();
	demo->GetSceneManager()->AddObject(obj);
	demo->GetSceneManager()->ListCurrentObjects();
	demo->Run();
	demo->Cleanup();

	CDummyDemo* demo2 = new CDummyDemo("Goodbye World Demo!");
	demo2->Setup();
	demo2->GetSceneManager()->AddObject(obj2);
	demo2->GetSceneManager()->ListCurrentObjects();
	demo2->Run();
	demo2->Cleanup();

	delete demo;
	demo = NULL;

	delete demo2;
	demo2 = NULL;

	obj = NULL;
	obj2 = NULL;
}

#endif