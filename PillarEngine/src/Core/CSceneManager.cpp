#include "CSceneManager.h"

CSceneManager::CSceneManager()
{
	
}

CSceneManager::~CSceneManager()
{

}

void CSceneManager::InitScene()
{
	if (!m_SceneObjects.empty())
	{
		for (int i = 0; i < m_SceneObjects.size(); i++)
		{
			m_SceneObjects[i]->Prepare();
		}
	}
}

void CSceneManager::DrawScene()
{
	for (int i = 0; i < m_SceneObjects.size(); i++)
	{
		m_SceneObjects[i]->Update();
	}
}

void CSceneManager::ClearObjects()
{
	if (!m_SceneObjects.empty())
	{
		for (int i = 0; i < m_SceneObjects.size(); i++)
		{
			m_SceneObjects[i]->Cleanup();
			delete m_SceneObjects[i];
			m_SceneObjects[i] = NULL;
		}
	}

	m_SceneObjects.clear();
}

void CSceneManager::AddObject(IObject* obj)
{
	// To-do: reconsider method of generating unique IDs for objects
	int counter = m_SceneObjects.size();
	obj->SetUID(counter+=1);
	m_SceneObjects.push_back(obj);
}

void CSceneManager::RemoveObject(int uid)
{
	// There's probably a more efficient way to do this
	for (int i = 0; i < m_SceneObjects.size();i++)
	{
		if (m_SceneObjects[i]->GetUID() == uid)
		{
			// Delete the object in memory, invalidate the pointer and erase it in our list
			IObject* tmp_ptr = m_SceneObjects[i];
			delete tmp_ptr;
			tmp_ptr = NULL;
			m_SceneObjects.erase((m_SceneObjects.begin()+i)-1);
		}
		else
		{
			printf("SceneManager Error: Object to remove does not exist.\n");
		}
	}
}

IObject* CSceneManager::GetObject(int objID)
{
	IObject* handle = NULL;

	// There's probably a more efficient way to do this
	for (int i = 0; i < m_SceneObjects.size();i++)
	{
		if (m_SceneObjects[i]->GetUID() == objID)
		{
			handle = m_SceneObjects[i];
		}
		else
		{
			printf("SceneManager Error: Object to retrieve does not exist.\n");
		}
	}

	return handle;
}

void CSceneManager::ListCurrentObjects()
{
	printf("\n---------------------------------------------------\nCurrent objects in scene:\n---------------------------------------------------\n");
	for (int i = 0;i < m_SceneObjects.size();i++)
	{
		printf("OBJECT ID: %i, TYPE: %s\n NAME: %s, DESC: %s \n", m_SceneObjects[i]->GetUID()
			   , m_SceneObjects[i]->GetType().c_str(), m_SceneObjects[i]->GetName().c_str()
			   , m_SceneObjects[i]->GetDesc().c_str());
	}
}