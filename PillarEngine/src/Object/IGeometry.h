#ifndef IGEOMETRY_H_
#define IGEOMETRY_H_

#include "global.h"
#include "IObject.h"

enum TransformChoice
{
	TRANSLATE = 1,
	ROTATE = 2,
	SCALE = 3
};

class IGeometry : public IObject
{
public:
	IGeometry() : IObject() { printf("Creating geometry\n"); };

	virtual ~IGeometry() {};
	virtual void Prepare() = 0;
	virtual void Update() = 0;
	virtual void Cleanup() = 0;

	// Consider pre-defining these in here
	virtual void Scale(vec3 scale) = 0;
	virtual void Translate(vec3 axis) = 0;
	virtual void Rotate(vec3 angle) = 0;
	virtual void SetTransform(vec3 transform, TransformChoice choice) = 0;

protected:
	mat4 m_modelMatrix; // Cumulative matrix to keep track of position, scale, orientation in word-space 
	vec3 m_scale;       // Model-space scale
	vec3 m_rotation;    // Model-space rotation/orientation

	GLuint m_VAO; // Vertex Array Object
	GLuint m_CBO; // Color Buffer 
	GLuint m_VBO; // Vertex Buffer
	GLuint m_IBO; // Index Buffer
	GLuint m_TBO; // Texture Buffer
	GLuint m_NBO; // Normal Buffer
};

#endif