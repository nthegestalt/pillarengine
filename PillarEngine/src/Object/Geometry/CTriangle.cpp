#include "CTriangle.h"

CTriangle::CTriangle() : IGeometry()
{
	SetName("Triangle");
	SetType("Geometry");
}

CTriangle::~CTriangle()
{

}

void CTriangle::Prepare()
{
	// Vertex Data
	GLfloat* vertices = new float[9];
	vertices[0] = -1.0; vertices[1] = -1.0;  vertices[2] = 0.0;
	vertices[3] = 1.0; vertices[4] = -1.0; vertices[5] = 0.0;
	vertices[6] = 0.0; vertices[7] = 1.0; vertices[8] = 0.0;

	// Color data
	GLfloat* colors = new float[9];
	colors[0] = 1.0f; colors[1] = 0.0f; colors[2] = 0.0f;
	colors[3] = 0.0f; colors[4] = 1.0f; colors[5] = 0.0f;
	colors[6] = 0.0f; colors[7] = 0.0f; colors[8] = 1.0f;

	// Generate buffer objects
	glGenVertexArrays(1, &m_VAO);
	glBindVertexArray(m_VAO);

	// VBO
	glGenBuffers(1, &m_VBO);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	glBufferData(GL_ARRAY_BUFFER, 9 * sizeof(GLfloat), vertices, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// CBO
	glGenBuffers(1, &m_CBO);
	glBindBuffer(GL_ARRAY_BUFFER, m_CBO);
	glBufferData(GL_ARRAY_BUFFER, 9 * sizeof(GLfloat), colors, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	// Load shaders
	GLSLShader vertex_shader;
	GLSLShader fragment_shader;

	if (vertex_shader.LoadShader("src//Demo//HelloWorld//simple.glsl.vert", SHADER_TYPE::VERTEX)) {
		m_shaderProgram.AddShader(vertex_shader);
	}

	if (fragment_shader.LoadShader("src//Demo//HelloWorld//simple.glsl.frag", SHADER_TYPE::FRAGMENT)) {
		m_shaderProgram.AddShader(fragment_shader);
	}

	m_shaderProgram.Link();

	// Cleanup
	delete vertices;
	vertices = NULL;

	delete colors;
	colors = NULL;
}

void CTriangle::Update()
{
	// Clear Depth/Z Buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);

	// Bind shader
	if (m_shaderProgram.RetrieveLinkStatus())
	{
		m_shaderProgram.Bind();
	}

	glm::mat4 Projection = glm::perspective(glm::radians(45.0f), (float)800.0f / (float)600.0f, 0.1f, 100.0f);
	glm::mat4 camera = glm::lookAt(glm::vec3(0, 0, -3), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	//m_modelMatrix *= glm::rotate(mat4(1.0), glm::radians(1.f), vec3(0, 1, 0));

	m_shaderProgram.SetUniform(Projection*camera*m_modelMatrix, "MVP");

	// Bind buffers
	glBindVertexArray(m_VAO);
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, m_CBO);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glDrawArrays(GL_TRIANGLES, 0, 3);

	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(0);
	glBindVertexArray(0);

	// Unbind shader
	if (m_shaderProgram.RetrieveLinkStatus())
	{
		m_shaderProgram.Unbind();
	}
}

void CTriangle::Cleanup()
{
	// Clean shaders
	if (m_shaderProgram.RetrieveLinkStatus())
	{
		m_shaderProgram.Cleanup();
	}

	glDeleteBuffers(1, &m_VBO);
	glDeleteBuffers(1, &m_CBO);
}

void CTriangle::Scale(vec3 scale)
{
	m_modelMatrix = glm::scale(m_modelMatrix, scale);
}

void CTriangle::Translate(vec3 axis)
{
	m_worldPosition += axis;
	m_modelMatrix *= glm::translate(mat4(1.0), axis);
}

void CTriangle::Rotate(vec3 angle)
{
	// Euler rotation
	float yaw = angle.x;   mat4 yaw_rot = glm::rotate(mat4(1), yaw, vec3(1, 0, 0));
	float pitch = angle.y; mat4 pitch_rot = glm::rotate(mat4(1), pitch, vec3(0, 1, 0));
	float roll = angle.z;  mat4 roll_rot = glm::rotate(mat4(1), roll, vec3(0, 0, 1));

	m_modelMatrix *= (yaw_rot * pitch_rot * roll_rot);
}

/**
	This function sets the transforms directly instead of utilizing an incremental approach
	like the other functions.
*/
void CTriangle::SetTransform(vec3 transform, TransformChoice choice)
{
	// To-Do: Apply manual popping/pushing of matrices here
	//
	// Note: 
	//   Reseting the matrix stack would be an expensive endeavour so
	//   we calculate the differences between the desired values and
	//   the current value to apply the changes.
	if (choice == TRANSLATE)
	{
		m_worldPosition = transform;
		m_modelMatrix *= glm::translate(mat4(1), transform);
	}
	else if (choice == ROTATE)
	{
			m_rotation = transform; // In Radians
			float yaw = transform.x;   mat4 yaw_rot = glm::rotate(mat4(1), yaw, vec3(1, 0, 0));
			float pitch = transform.y; mat4 pitch_rot = glm::rotate(mat4(1), pitch, vec3(0, 1, 0));
			float roll = transform.z;  mat4 roll_rot = glm::rotate(mat4(1), roll, vec3(0, 0, 1));
			
			m_modelMatrix *= (yaw_rot * pitch_rot * roll_rot);
	}
	else if (choice == SCALE)
	{
		m_scale = transform;
		m_modelMatrix *= glm::scale(mat4(1), transform);
	}
	else
	{
		printf("Error: Invalid transform choice!");
	}
}