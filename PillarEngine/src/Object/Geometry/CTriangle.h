#ifndef CTRIANGLE_H_
#define CTRIANGLE_H_

#include "IGeometry.h"
#include "global.h"
#include "GLSLShader.h"
#include "GLSLProgram.h"


class CTriangle : public IGeometry
{
public:
	CTriangle();
	virtual ~CTriangle();

	void Prepare();
	void Update();
	void Cleanup();
	void Scale(vec3 scale);
	void Translate(vec3 axis);
	void Rotate(vec3 angle);
	void SetTransform(vec3 transform, TransformChoice choice);

private:
	GLSLProgram m_shaderProgram;
};

#endif