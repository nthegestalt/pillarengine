#ifndef CDUMMYOBJECT_H_
#define CDUMMYOBJECT_H_

#include "IGeometry.h"
#include "global.h"
#include "GLSLShader.h"
#include "GLSLProgram.h"


class CDummyObj : public IObject
{
public:
	CDummyObj() : IObject() { printf("Creating Dummy Object!\n"); };
	virtual ~CDummyObj() {};

	void Prepare() { printf("Creating Dummy Object! \n"); };
	void Update() { printf("Creating Dummy Object!\n"); };
	void Cleanup() { printf("Creating Dummy Object!\n"); };

};

#endif