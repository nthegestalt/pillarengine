#ifndef IOBJECT_H_
#define IOBJECT_H_

#include "global.h"

/**
	An interface-like class to handle all objects through our scene manager. 
	The geomtry details and such forth will be different, but they all have 
	in common the tasks of Initialisation, Rendering and Cleanup.
*/

class IObject
{
public:
	IObject(){ printf("Creating object\n"); };
	virtual ~IObject() {};
	virtual void Prepare() = 0;
	virtual void Update() = 0;
	virtual void Cleanup() = 0;

	void SetUID(int uid) { m_uid = uid; }
	void SetType(string type) { m_type = type; }
	void SetName(string name) { m_name = name; }
	void SetDescription(string description) { m_description = description; }
	
	int GetUID() { return m_uid; }
	string GetName() { return m_name; }
	string GetType() { return m_type; }
	string GetDesc() { return m_description; }
	vec3   GetPosition() { return m_worldPosition; }

protected:
	int m_uid;
	string m_name;
	string m_type;
	string m_description;
	vec3   m_worldPosition; // Position in world-space
};

#endif