#pragma once
/**
	A list of essential includes
*/
#include <stdlib.h>
#include <stdio.h>
#include <Windows.h>
#include <iostream>
#include <string>
#include <functional>
#include <GL\glew.h>
#include <GLFW\glfw3.h>
#include <glm.hpp>
#include <glm\gtc\matrix_transform.hpp>

// Math functions
using glm::mat3;
using glm::mat4;
using glm::vec4;
using glm::vec3;
using glm::vec2;
using std::string;