/**
	Simple shader to output an object in world space.
	Author: Tuan
	Last Modified: 28th June 2016
*/
#version 330 core

layout(location = 0) in vec3 vertexPosition_modelSpace;
layout(location = 1) in vec3 vertexColor_model;

uniform mat4 MVP;

out vec3 pass_color;

void main()
{
	gl_Position = MVP * vec4(vertexPosition_modelSpace,1);
	pass_color = vertexColor_model;
}