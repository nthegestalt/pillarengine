/**
	Simple shader to output an object's color properties.
	Author: Tuan
	Last Modified: 28th June 2016
*/

#version 330 core

in vec3 pass_color;

out vec3 out_color;

void main()
{
  // Output color to white
  out_color = pass_color;
}

