#ifndef CHELLOWORLD_H_
#define CHELLOWORLD_H_

#include "global.h"
#include "IDemo.h"
#include "Object\Geometry\CTriangle.h"
#include "DirtyIO.h"

class CHelloWorldDemo : public IDemo 
{
public:
	CHelloWorldDemo(string name);
	virtual ~CHelloWorldDemo();

	void Setup();
	void Run();
	void Cleanup();
	void Keyboard(GLFWwindow* window);
	void Mouse(GLFWwindow* window);

private:
	KeyObj m_ScnLstkey;
};

#endif