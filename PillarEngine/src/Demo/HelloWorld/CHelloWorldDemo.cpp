#include "CHelloWorldDemo.h"
#include "DirtyIO.h"

CHelloWorldDemo::CHelloWorldDemo(string name) : IDemo(name)
{

}

CHelloWorldDemo::~CHelloWorldDemo()
{

}

void CHelloWorldDemo::Setup()
{
	// Call it once to initialise the scene manager
	GetSceneManager();

	// Create some objects to populate the scene
	CTriangle* triangle1 = new CTriangle();
	triangle1->SetName("Triangle One");
	triangle1->SetDescription("Illuminati Triangle!");

	GetSceneManager()->AddObject(triangle1);
	IObject* handle = GetSceneManager()->GetObject(1);

	// Dynamic cast from Parent interface to child geometry interface
	if (handle->GetType() == "Geometry")
	{
		// The casting...it isn't supposed to work!
		dynamic_cast<IGeometry*>(handle)->SetTransform(vec3(0.25, 0.25, 0.25), SCALE);
		//dynamic_cast<IGeometry*>(handle)->SetTransform(vec3(1, 0, 0), TRANSLATE);
		//dynamic_cast<IGeometry*>(handle)->SetTransform(vec3(45, 0, 0), ROTATE);
	}

	GetSceneManager()->InitScene();

	// Assign some special keyboard bindings
	m_ScnLstkey.isPressed = false;
	m_ScnLstkey.number = GLFW_KEY_L;
}

bool updown = false;
void CHelloWorldDemo::Run()
{
	double time = glfwGetTime();
   IGeometry* handle = dynamic_cast<IGeometry*>(GetSceneManager()->GetObject(1));

	handle->Rotate(vec3(0, glm::radians(1.0),0));
	
	// Bounce the triangle up and down
	if (updown == true)
	{
		handle->Translate(vec3(0, 0.05, 0));
		if (handle->GetPosition().y >= 3)
		{
			updown = false;
		}
	}
	else
	{
		handle->Translate(vec3(0, -0.05, 0));
		if (handle->GetPosition().y <= -3)
		{
			updown = true;
		}
	}

	GetSceneManager()->DrawScene();
}

void CHelloWorldDemo::Cleanup()
{
	DeleteSceneManager();
}

void CHelloWorldDemo::Keyboard(GLFWwindow* window)
{
	// Pre-C++ 11: Create a member-function pointer
	//void(CSceneManager::* p)(void) = &CSceneManager::ListCurrentObjects;

	// C++ 11: Bind a member function to a function pointer
	auto fp_scnLst = std::bind(&CSceneManager::ListCurrentObjects, GetSceneManager());
	KeyPressOnce(window, m_ScnLstkey, fp_scnLst);
}

void CHelloWorldDemo::Mouse(GLFWwindow* window)
{
	// Nothing for now
}