#ifndef CDIAMONDSQUAREDEMO_H_
#define CDIAMONDSQUAREDEMO_H_

#include "global.h"
#include "IDemo.h"

class CDiamondSquareDemo : public IDemo
{
public:
	CDiamondSquareDemo(string name);
	virtual ~CDiamondSquareDemo();

	void Setup();
	void Run();
	void Cleanup();
	void Keyboard();
	void Mouse();

private:
	void DiamondSquare();
	void SaveHeightData();
};

#endif