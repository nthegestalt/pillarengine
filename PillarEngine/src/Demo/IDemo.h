#ifndef IDEMO_H_
#define IDEMO_H_

#include <string>
#include "CSceneManager.h"
#include "global.h"

class IDemo
{
public:
	IDemo(string name) : m_demoName(name) {}

	virtual ~IDemo() {};
	virtual void Setup() = 0;
	virtual void Run() = 0;
	virtual void Cleanup() = 0;
	virtual void Keyboard(GLFWwindow* window) = 0;
	virtual void Mouse(GLFWwindow* window) = 0;
	
public:
	void   SetName(string name) { m_demoName = name; }
	string GetName() { return m_demoName; }
	static CSceneManager* GetSceneManager();
	static void DeleteSceneManager();

protected:
	string m_demoName;

private:
	static CSceneManager* m_sceneManager;
};

#endif