#include "IDemo.h"

/**
	Encapsulated singleton instance of the scene manager. We can quickly if need to be, query other objects 
	within the scene from other objects if we need to do something i.e. grab location of a light source from 
	a light object to operate on a certain object/scene. We don't want for only one scene manager to exist
	hence, by having a single instance per demo created, we can also have different managers for different demos. 
*/

CSceneManager* IDemo::m_sceneManager = NULL;

// Retrieve singleton instance
CSceneManager* IDemo::GetSceneManager()
{
	if (m_sceneManager == NULL)
	{
		m_sceneManager = new CSceneManager();
	}

	return m_sceneManager;
}

// Delete scene manager instance
void IDemo::DeleteSceneManager()
{
	// Safeguard: Ensure we have no objects left before deleting the manager. 
	//	---------- This is incase we do something stupid or need to create a
	//				  new scene manager in it's place. It's a bit awkward though,
	//				  perhaps the overall design needs to be reconsidered?

	if (m_sceneManager != NULL)
	{
		m_sceneManager->ClearObjects();

		delete m_sceneManager;
		m_sceneManager = NULL;
	}
}