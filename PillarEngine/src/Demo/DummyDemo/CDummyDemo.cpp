#include "CDummyDemo.h"

CDummyDemo::CDummyDemo(string name) : IDemo(name)
{
	printf("Running Dummy Demo Constructor...\n");
}

CDummyDemo::~CDummyDemo()
{
	printf("Running Dummy Demo Destructor...\n");
}

void CDummyDemo::Setup()
{
	// Call it once to initialise the scene manager
	GetSceneManager();
}

void CDummyDemo::Run()
{
	printf("Running Dummy Demo...\n");
}

void CDummyDemo::Cleanup()
{
	printf("Shutting down Dummy Demo...\n");
	DeleteSceneManager();
}

void CDummyDemo::Keyboard(GLFWwindow* window)
{
	printf("Running Dummy Demo Keyboard...\n");
}

void CDummyDemo::Mouse(GLFWwindow* window)
{
	printf("Running Dummy Demo Mouse...\n");
}