#ifndef CDUMMYDEMO_H_
#define CDUMMYDEMO_H_

#include "global.h"
#include "IDemo.h"

class CDummyDemo : public IDemo
{
public:
	CDummyDemo(string name);
	virtual ~CDummyDemo();

	void Setup();
	void Run();
	void Cleanup();
	void Keyboard(GLFWwindow* window);
	void Mouse(GLFWwindow* window);
};

#endif