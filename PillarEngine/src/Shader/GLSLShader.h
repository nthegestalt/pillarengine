#ifndef GLSLSHADER_H_
#define GLSLSHADER_H_

#include "global.h"

// List of possible shaders we can make
enum SHADER_TYPE
{
	VERTEX = 0,
	FRAGMENT = 1,
	GEOMETRY = 2
};

/*************************************************
	Class Name: GLSLShader
	Desc: Handles creation of shaders
**************************************************/

class GLSLShader
{
private:
	GLuint			m_shaderHandle;
	
public:
					GLSLShader();
					~GLSLShader();

	bool			LoadShader(string source, SHADER_TYPE);
	bool			LoadShader(char* file, SHADER_TYPE);
	GLuint			RetrieveHandle();
	void			DeleteShaders();

private:
	char*			ReadText(char* file);
	void			PrintLog();
	
};

#endif