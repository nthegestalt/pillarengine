#include "GLSLProgram.h"

GLSLProgram::GLSLProgram()
{
}

GLSLProgram::~GLSLProgram()
{
}

/*************************************************
	Name: AddShader
	Desc: Adds a shader to compile and link into
		  the program
	Params: GLSLShader
**************************************************/
void GLSLProgram::AddShader(GLSLShader s)
{
	m_shaderList.push_back(s);
}

/*************************************************
	Name: Links
	Desc: Links the shaders to make a program
	Params: Nil
**************************************************/
void GLSLProgram::Link()
{
	m_compiled = false;
	m_programHandle = glCreateProgram();

	if(m_shaderList.size() != 0){
		for(int i = 0; i < m_shaderList.size(); i++){
			glAttachShader(m_programHandle, m_shaderList[i].RetrieveHandle());
		}

		glLinkProgram(m_programHandle);

		GLint linked;
		glGetShaderiv(m_programHandle, GL_LINK_STATUS, &linked);
		if(linked == GL_FALSE){
			printf("Unabled to link program.\n");
			PrintLog();
			m_compiled = false;
			return;
		}
	}else{
		printf("There are no shaders to link.\n");
		m_compiled = false;
		return;
	}
	m_compiled = true;
}

/*************************************************
	Name: Bind
	Desc: Binds the program for usage
	Params: Nil
**************************************************/
void GLSLProgram::Bind()
{
	glUseProgram(m_programHandle);
}

/*************************************************
	Name: Unbind
	Desc: Unbinds the program
	Params: Nil
**************************************************/
void GLSLProgram::Unbind()
{
	glUseProgram(0);
}

/*************************************************
	Name: PrintLog
	Desc: Prints the program log
	Params: Nil
**************************************************/
void GLSLProgram::PrintLog()
{
	int infologLength = 0;
    int charsWritten  = 0;
    char *infoLog;
 
    glGetProgramiv(m_programHandle, GL_INFO_LOG_LENGTH, &infologLength);
 
    if (infologLength > 0)
    {
        infoLog = (char *)malloc(infologLength);
        glGetProgramInfoLog(m_programHandle, infologLength, &charsWritten, infoLog);
		printf("%s\n",infoLog);
        free(infoLog);
    }
}

/*************************************************
	Name: Cleanup
	Desc: Cleans up after we've finished
	Params: Nil
**************************************************/
void GLSLProgram::Cleanup()
{
	for(int i = 0; i < m_shaderList.size(); i++){
		glDetachShader(m_programHandle, m_shaderList[i].RetrieveHandle());
		m_shaderList[i].DeleteShaders();
	}
	glDeleteProgram(m_programHandle);
}

/*************************************************
	Name: RetrieveLinkStatus
	Desc: Checks if the program has been linked properly
	Params: Nil
**************************************************/
bool GLSLProgram::RetrieveLinkStatus()
{
	return m_compiled;
}

/*************************************************
|			 DATA SETTING METHODS				 |
**************************************************/

/*************************************************
	Name: SetUniform (Overload - int) 
	Desc: Sets a uniform variable in shader
	Params: GLint amount, string data name
**************************************************/
void GLSLProgram::SetUniform(GLint i, string name)
{
	GLuint loc = glGetUniformLocation(m_programHandle, name.c_str());
	
	if(loc < 0){
		printf("Failed to set location. Check if variable exists.\n");
		return;
	}else{
		glProgramUniform1i(m_programHandle,loc,i);
	}
}

/*************************************************
	Name: SetUniform (Overload - float) 
	Desc: Sets a uniform variable in shader
	Params: GLfloat amount, string data name
**************************************************/
void GLSLProgram::SetUniform(GLfloat f, string name)
{
	GLuint loc = glGetUniformLocation(m_programHandle, name.c_str());
	
	if(loc < 0){
		printf("Failed to set location. Check if variable exists.\n");
		return;
	}else{
		glProgramUniform1f(m_programHandle,loc,f);
	}
}

/*************************************************
	Name: SetUniform (Overload - vector(2)) 
	Desc: Sets a uniform variable in shader
	Params: vec2 vector, string data name
**************************************************/
void GLSLProgram::SetUniform(vec2 v, string name)
{
	GLuint loc = glGetUniformLocation(m_programHandle, name.c_str());
	
	if(loc < 0){
		printf("Failed to set location. Check if variable exists.\n");
		return;
	}else{
		glUniform2f(loc,v.x,v.y);
	}
}

/*************************************************
	Name: SetUniform (Overload - vector(3)) 
	Desc: Sets a uniform variable in shader
	Params: vec3 vector, string data name
**************************************************/
void GLSLProgram::SetUniform(vec3 v, string name)
{
	GLuint loc = glGetUniformLocation(m_programHandle, name.c_str());
	
	if(loc < 0){
		printf("Failed to set location. Check if variable exists.\n");
		return;
	}else{
		glUniform3f(loc,v.x,v.y,v.z);
	}
}

/*************************************************
	Name: SetUniform (Overload - matrix(3x3)) 
	Desc: Sets a uniform variable in shader
	Params: mat3 matrix, string data name
**************************************************/
void GLSLProgram::SetUniform(mat3 m, string name)
{
	GLuint loc = glGetUniformLocation(m_programHandle, name.c_str());
	
	if(loc < 0){
		printf("Failed to set location. Check if variable exists.\n");
		return;
	}else{
		glUniformMatrix3fv(loc,1,GL_FALSE,&m[0][0]);
	}
}

/*************************************************
	Name: SetUniform (Overload - vector(4)) 
	Desc: Sets a uniform variable in shader
	Params: vec4 vector, string data name
**************************************************/
void GLSLProgram::SetUniform(vec4 v, string name)
{
	GLuint loc = glGetUniformLocation(m_programHandle, name.c_str());
	
	if(loc < 0){
		printf("Failed to set location. Check if variable exists.\n");
		return;
	}else{
		glUniform4f(loc,v.x,v.y,v.z, v.w);
	}
}

/*************************************************
	Name: SetUniform (Overload - matrix(4x4)) 
	Desc: Sets a uniform variable in shader
	Params: mat3 matrix, string data name
**************************************************/
void GLSLProgram::SetUniform(mat4 m, string name)
{
	GLuint loc = glGetUniformLocation(m_programHandle, name.c_str());
	
	if(loc < 0){
		printf("Failed to set location. Check if variable exists.\n");
		return;
	}else{
		glUniformMatrix4fv(loc,1,GL_FALSE,&m[0][0]);
	}
}