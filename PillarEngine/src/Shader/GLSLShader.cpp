#include "GLSLShader.h"

GLSLShader::GLSLShader()
{
}

GLSLShader::~GLSLShader()
{
}

/*************************************************
	Name: LoadShader (overload)
	Desc: Loads a GLSL shader given a file
	Params: filename
**************************************************/
bool GLSLShader::LoadShader(char* fileName, SHADER_TYPE s)
{
	char* src;
	src = ReadText(fileName);
	const char *csrc = src;

	// Create shader based on type input
	if(s == VERTEX){
		m_shaderHandle = glCreateShader(GL_VERTEX_SHADER);
	}else if(s == FRAGMENT){
		m_shaderHandle = glCreateShader(GL_FRAGMENT_SHADER);
	}else if(s == GEOMETRY){
		m_shaderHandle = glCreateShader(GL_GEOMETRY_SHADER);
	}

	if(src != NULL){
		glShaderSource(m_shaderHandle,1,&csrc,NULL);
		free(src);

		glCompileShader(m_shaderHandle);

		GLint compiled;
	
		glGetShaderiv(m_shaderHandle, GL_COMPILE_STATUS, &compiled);
		if(compiled == GL_FALSE){
			printf("Failed to compile shader. Check logs! \n");
			PrintLog();
			return false;
		}
	}else{
		printf("Unable to read source.\n");
		return false;
	}
	return true;
}

/*************************************************
	Name: LoadShader (overload)
	Desc: Loads a GLSL shader given a source
	Params: source string
**************************************************/
bool GLSLShader::LoadShader(string source, SHADER_TYPE s)
{
	const char* src = source.c_str();

	// Create shader based on type input
	if(s == VERTEX){
		m_shaderHandle = glCreateShader(GL_VERTEX_SHADER);
	}else if(s == FRAGMENT){
		m_shaderHandle = glCreateShader(GL_FRAGMENT_SHADER);
	}else if(s == GEOMETRY){
		m_shaderHandle = glCreateShader(GL_GEOMETRY_SHADER);
	}

	if(src != NULL){
		glShaderSource(m_shaderHandle,	1, &src, NULL);

		GLint compiled = 0;

		glGetShaderiv(m_shaderHandle, GL_COMPILE_STATUS, &compiled);
		if(compiled == GL_FALSE){
			printf("Failed to compile shader. Check logs! \n");
			PrintLog();
			return false;
		}
	}else{
		printf("Unable to read source.\n");
		return false;
	}

	return true;
}

/*************************************************
	Name: RetrieveHandle
	Desc: Returns shader handle
	Params: Nil
	Returns: GLuint shaderhandle
**************************************************/
GLuint GLSLShader::RetrieveHandle()
{
	return m_shaderHandle;
}

/*************************************************
	Name: ReadText
	Desc: Parses a text file
	Params: filename
**************************************************/
char* GLSLShader::ReadText(char* filename)
{
	FILE* fp;
	char* content = NULL;

	int count = 0;

	// Check if file exists
	if(filename != NULL){
		fp = fopen(filename,"rt");

		// Check for empty file then parse
		if(fp != NULL){
			fseek(fp, 0, SEEK_END);
			count = ftell(fp);
			rewind(fp);

			if(count > 0){
				content = (char*)malloc(sizeof(char) * (count + 1));
				count = fread(content,sizeof(char),count,fp);
				content[count] = '\0';
			}else{
				fclose(fp);
				printf("File '%s' is Empty.\n", filename);
				return NULL;
			}
			fclose(fp);
		}else{
			printf("No such file exists.\n");
			return NULL;
		}

	}

	return content;
}


/*************************************************
	Name: PrintLog
	Desc: Prints shader error log if any
	Params: Nil
**************************************************/
void GLSLShader::PrintLog()
{
	int infoLength = 0;
	int charsWritten = 0;
	char* infoLog;
	
	glGetShaderiv(m_shaderHandle, GL_INFO_LOG_LENGTH, &infoLength);

	if(infoLength > 0){
		infoLog = (char *)malloc(infoLength);
		glGetShaderInfoLog(m_shaderHandle, infoLength, &charsWritten, infoLog);
		printf("%s\n",infoLog);
		free(infoLog);
	}
}

/*************************************************
	Name: DeleteShader
	Desc: Cleans up after we've finished with them
	Params: Nil
**************************************************/
void GLSLShader::DeleteShaders()
{
	if(m_shaderHandle != NULL)
		glDeleteShader(m_shaderHandle);
}
