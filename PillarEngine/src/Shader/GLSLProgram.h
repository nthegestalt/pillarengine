#ifndef GLSLPROGRAM_H_
#define GLSLPROGRAM_H_

#include "global.h"
#include "GLSLShader.h"
#include <vector>

using std::vector;
using glm::vec2;
using glm::vec3;
using glm::vec4;
using glm::mat3;
using glm::mat4;

/*************************************************
	Class Name: GLSLProgram
	Desc: Manages shaders and links them to create 
		  a GLSL program.
**************************************************/

class GLSLProgram
{
private:
	vector<GLSLShader>			m_shaderList;
	GLuint							m_programHandle;
	bool								m_compiled;
public:
								GLSLProgram();
								~GLSLProgram();
	
	void						Link();
	void						AddShader(GLSLShader s);							
	void						Bind();
	void						Unbind();
	void						PrintLog();
	void						Cleanup();

	bool						RetrieveLinkStatus();

	// DATA SETTING
	void						SetUniform(GLint i, string dataName);
	void						SetUniform(GLfloat f, string dataName);
	void						SetUniform(vec2 v, string dataName);
	void						SetUniform(vec3 v, string dataName);
	void						SetUniform(mat3 m, string dataName);
	void						SetUniform(vec4 v, string dataName);
	void						SetUniform(mat4 m, string dataName);

};

#endif