#include "CEngine.h"
#include "Core\UnitTests\CoreFunctionTests.h"
#include "Demo\HelloWorld\CHelloWorldDemo.h"
#include "vld.h"

void main()
{
	CEngine* engine = CEngine::GetEngine(800, 600, 3, 3, "Demo Engine");
	CHelloWorldDemo* hello_demo = new CHelloWorldDemo("Hello World Demo!");

	engine->AttachDemo(hello_demo);
	engine->Setup(false);
	engine->Run();
}